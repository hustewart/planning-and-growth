#### How this issue works

Each Monday a new ~planning issue is created.

Comments on the issue are divided by weekday names. This allows a plan for each day. Also document stand up messages in Slack here, because the Slack messages are ephemeral.

Optionally add a `###Next Week` comment.

/label ~planning
/due in 7 days
/todo
